$LOAD_PATH.unshift(File.expand_path(File.dirname(__FILE__)), 'lib')

require 'grpc'
require 'hello_services_pb'

class HelloServer < Hello::Service
  def say_hello(hello_request, _call)
    HelloResponse.new(message: "hello #{hello_request.name}, you are #{hello_request.age} years old")
  end
end

s = GRPC::RpcServer.new
s.add_http2_port('0.0.0.0:50051', :this_port_is_insecure)
s.handle(HelloServer)
s.run_till_terminated
