# Generated by the protocol buffer compiler.  DO NOT EDIT!
# Source: hello.proto for package ''

require 'grpc'
require 'hello_pb'

module Hello
  class Service

    include GRPC::GenericService

    self.marshal_class_method = :encode
    self.unmarshal_class_method = :decode
    self.service_name = 'Hello'

    rpc :SayHello, HelloRequest, HelloResponse
  end

  Stub = Service.rpc_stub_class
end
