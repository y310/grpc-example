$LOAD_PATH.unshift(File.expand_path(File.dirname(__FILE__)))

require 'grpc'
require 'hello_services_pb'

stub = Hello::Stub.new('localhost:50051', :this_channel_is_insecure)
p stub.say_hello(HelloRequest.new(name: 'alice', age: 15))
